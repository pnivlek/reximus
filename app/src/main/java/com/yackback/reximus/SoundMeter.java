package com.yackback.reximus;

import android.content.Context;
import android.media.MediaRecorder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import es.dmoral.toasty.Toasty;

/**
 * Created by 18kporter on 2/8/2018.
 */

public class SoundMeter {

    private final static String TAG = "Reximus/SoundMeter";
    private boolean activated = false;
    private MediaRecorder mRecorder;
    private double amp;
    private Context mContext;

    public SoundMeter(Context mContext) {
        this.mContext = mContext;
    }

    public void init() {
        Log.d(TAG, "init() initialized");

    }

    protected void start() {

    }



    public void stop() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    double getAmplitude() {
        if (mRecorder != null)
            return  mRecorder.getMaxAmplitude();
        else
            return 0;
    }

    boolean isActivated(){
        return activated;
    }
    void setActivated(boolean activated) {
        this.activated = activated;
    }

    int makeAdjustment(double amp) {
        Log.i("REXIMUS/AmplitudeLog", "Current amp:" + amp);
        Toasty.info(mContext, "amp is equal to" + amp, Toast.LENGTH_SHORT).show();
        return 0; // return a standard exit code
    }
}