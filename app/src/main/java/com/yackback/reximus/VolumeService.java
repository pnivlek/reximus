package com.yackback.reximus;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import es.dmoral.toasty.Toasty;

/**
 * Created by 18kporter on 3/21/2018.
 */

public class VolumeService extends Service {
    private static MediaRecorder mRecorder = null;
    private SoundMeter mMeter = new SoundMeter(this);
    private static final String TAG = "Reximus/VolumeService";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
        }
        if (mRecorder != null) {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            Log.d(TAG, "Set recording audio source to UNPROCESSED");
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            Log.d(TAG, "Set recording output format to THREE_GPP");
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            Log.d(TAG, "Set audio encoder to AMR_NB");
            mRecorder.setOutputFile("/dev/null");
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Toasty.info(getApplicationContext(), "onCreate of VolumeService done", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mRecorder.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    mMeter.makeAdjustment(mMeter.getAmplitude());
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Interrupted getAmplitude Thread");
                        e.printStackTrace();
                    }
                }
            }
        });
        return START_STICKY;
    }
}
